var uuid = require('uuid');
var Events = require('../utility/events');

module.exports = function(cardData, resource) {
  var title = cardData.title;
  var description = cardData.description;
	var type = cardData.type;	
	var quality = cardData.quality;	
  var cardId = uuid();

  var displayData;
		
  var events = Events(cardData.events);
  
	function getDisplayData() {
		if (!displayData) {
			displayData = resource.processObject({
			  title: title,
				description: description,
        type: type,
        quality: quality,
        id: cardId
			});
		}
	  return displayData;
	}
  
  return {
    events: events,
	  getDisplayData: getDisplayData
  };
};
