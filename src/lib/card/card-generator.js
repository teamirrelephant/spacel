var _ = require('underscore');

var Card = require('./card'); 

module.exports = function CardGenerator(resource, cardLibrary) {
  var prebuiltCards = {};
	
	var haveToGenerate = 5;
  
  function getPrebuiltCards(player) {
    var cards = [];
    if (prebuiltCards[player]) {
      while(cards.length != haveToGenerate || prebuiltCards[player].length != 0)
        cards.push(prebuiltCards[player].shift());
    }
    return cards;
  }
  
  function nextCard() {
		return Card(_.sample(cardLibrary.rawCards), resource);
  }
  
  return {    
    peekCards: function(amount, player) {
      var peekedCards = [];
      if (amount <= 0) return peekedCards;
      
      while (amount--) peekedCards.push(nextCard());
      prebuiltCards[player] = _.union(prebuiltCards[player], peekedCards);
      
      return peekedCards;
    },
    
    generateHand: function(player) {
      var newHand = getPrebuiltCards(player);
      while (newHand.length != haveToGenerate) 
        newHand.push(nextCard());
      player.setHand(newHand);
    }
  };
};
