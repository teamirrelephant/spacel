var fs = require('fs');
var _ = require('underscore');

module.exports = function(searchDirectories, cardPacks, resource) {
	var cardData = [];
	
	function buildPath(directory, pack) {
		return directory.trim('/') + '/' + pack + '.cardpack.json';
	}
	
  function prepareCardEvents(eventsObject) {
    for (event in eventsObject) {
			var handlerFunction = new Function('context, displayData', resource.processString(eventsObject[event]));
		  eventsObject[event] = [];
			eventsObject[event].push(handlerFunction);
    }
  }
		
	function addCardsToLibrary(cards, resources) {
		cards.forEach(function(card) {
      prepareCardEvents(card.events);
      cardData.push(card);
    });
  }

	function buildResourcePath(cardpackPath, resourceName) {
		return cardpackPath.substring(0, cardpackPath.lastIndexOf('/')) + '/' + resourceName + '.resource.json';
	}

	function printCardpackData(cardpack) {
    console.log('Found cardpack "' + cardpack.name + '"');
		console.log('Author: ' + cardpack.author);
		console.log('Using ' + cardpack.resourceFiles.length + ' resouce files.');	
    console.log('Adding ' + cardpack.cards.length + ' cards.');
	}
		
	function tryLoadCardPack(path) {
		try {
			var cardpack = JSON.parse(fs.readFileSync(path, 'utf8'));
			printCardpackData(cardpack);	
			cardpack.resourceFiles.forEach(function(resourceName) { resource.loadResource(buildResourcePath(path, resourceName)); });	
			addCardsToLibrary(cardpack.cards);
    } catch (error) {
				console.log('Error has occured while loading cardpack @ ' + path);
				console.log(error);
		}
	}
	
	cardPacks.forEach(function(pack) {
	  searchDirectories.forEach(function(directory) {
		  tryLoadCardPack(buildPath(directory, pack));				
		});
	});
  
  return {
    rawCards: cardData
  };
};
