var _ = require('underscore');

var CardGenerator = require('./card/card-generator');
var UserInterface = require('./user-interface');

module.exports = function(resource, cardGenerator) {
  var players = [];
  
  var gameObjectsCache = {};

  var gameState = {
    currentPlayer: 0,
    turn: 0,
    playsLeft: 3
	};
  
  function updateCurrentPlayer() {
  	gameState.currentPlayer++;
  	if (players.length == gameState.currentPlayer) {
      gameState.currentPlayer = 0;
      gameState.turn++;
    }
  }
  
  function sortPlayers() {
    _.shuffle(players);
  	_.sortBy(players, function(player) { return player.ship.speed; });
  }

  function getCurrentPlayer() {
    return players[gameState.currentPlayer];
  }

  function buildGameContext(targetId, playedCard) {
    return {
      allies: _.filter(players, function(player) { return player.team === getCurrentPlayer().team; }),
      enemies: _.reject(players, function(player) { return player.team === getCurrentPlayer().team; }),
      players: players,
      player: getCurrentPlayer(),
      target: getInGameObject(targetId),
      cardGenerator: cardGenerator,
      playedCard: playedCard
    };
  }
  
  function multipleTeamsPresent() {
    return _.uniq(_.map(players, function(player) { return player.team; })).length > 1;
  }
  
  function getInGameObject(id) {
    if (gameObjectsCache[id]) { return gameObjectsCache[id]; }
    
    var found;
    players.some(function(player) {
      found = player.getInGameObject(id);
      if (found) return true;
    });
    
    gameObjectsCache[id] = found;
    return found;
  }
  
  function playCardById(cardId, targetId) {
    var playedCard = _.find(getCurrentPlayer().hand, function(card) { return card.id === cardId; });
    getCurrentPlayer().hand.splice(getCurrentPlayer().hand.indexOf(playedCard), 1);
    delete gameObjectsCache[cardId];
    playedCard.events.trigger('played', buildGameContext(targetId, playedCard));
  }

  function checkGameConditions() {
    
  }

  return {
  	addPlayer: function(player) {
  	  players.push(player);
  	},

    start: function() {
      sortPlayers();
      gameState.turn = gameState.currentPlayer = 0;
    },
    
    playCard: function(player, cardId, targetId) {
      if (player == getCurrentPlayer()) {
        playCardById(cardId, targetId);
        checkGameConditions();
        return this.state;
      } else {
        console.log('[Warning]'.orange + ' Player tried to make a move on another players turn.');
        return 'Wait for your turn!';
      }
    },
    
    get state() { return JSON.parse(JSON.stringify(gameState)); }
  };
};
