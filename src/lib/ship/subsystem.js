module.exports = function(subsystemData) {
	var name = subsystemData.name;
	var type = subsystemData.type;

	return {
		name: name,
		type: type
	};
}
