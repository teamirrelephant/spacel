exports.roomDamage = {
	ok: 1,
	slightlyDamaged: 0.9,
	damaged: 0.6,
	critical: 0.2,
	destroyed: 0
};
