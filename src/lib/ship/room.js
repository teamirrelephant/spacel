var _ = require('underscore');

var Subsystem = require('./subsystem');
var enums = require('./enums');

module.exports = function(roomData) {
	var name = roomData.name;
	var subsystems = _.map(roomData.subsystems, Subsystem);
	var state = roomData.damage;
	var defence = {
		kinetic: roomData.defence.kinetic || 0.1,
		thermal: roomData.defence.thermal || 0.1,
		em: roomData.defence.em || 0.1
	};

	var cargo = roomData.cargo || [];
	var cargoSlots = roomData.cargoSlots;
	
	return {
		name: name,
		subsystems: subsystems,
		currentDamage: state,
		defence: defence
	};
}
