var _ = require('underscore');
var uuid = require('uuid');

var Room = require('./room');

module.exports = function(shipData) {
  var events = require('../utility/events')({});
	
  /**
  * Core logic
  */
	var name = shipData.name;
	var stats = {
		speed: shipData.speed,
		signature: shipData.signature,
		minIntegrity: shipData.minIntegrity,
		defence: {
			kinetic: shipData.defence.kinetic,
			thermal: shipData.defence.thermal,
			em: shipData.defence.em
		}
	};
  
  function recalculateShipIntegrity() {
    return 1;
  }
		
	var rooms = _.map(shipData.rooms, Room);
	var integrity = recalculateShipIntegrity();
		
	function getWeapons() {
		var weapons = [];			
		rooms.forEach(function(room) {
			weapons = _.union(_.filter(room.subsystems, function(subsystem) { subsystem.type == 'weapon'; }));	
		});
	  return weapons;
	}
	
	return {
    id: uuid(),
		name: name,
	  stats: stats,
		rooms: rooms,	
    integrity: integrity,
		get weapons() { return getWeapons(); }    
	};		
};
