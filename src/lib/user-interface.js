var colors = require('colors');
var readlineSync = require('readline-sync');
var _ = require('underscore');

module.exports = function(resource, game) {  
  return {
    init: function() {
          
    },
    
    anounceTurn: function(turn) {
      console.log('Starting ' + ('turn ' + turn).green + '!');
    },
    
    anouncePlayer: function(player) {
      var playerName = player.name;
      if (colors[player.team]) {
        playerName = playerName + ' (' + colors[player.team](player.team) + ')';
      } else {
        playerName = playerName + ' (' + player.team + ')';
      }
      
      console.log(playerName + '\'s turn!');
    },
       
    playCard: function(gameCtx) {
      
    },
    
		selectCard: function(hand, cardsLeft) {	
			var cardTitles = _.map(hand, function(card) { return card.getDisplayData().title; });
      var index = readlineSync.keyInSelect(cardTitles, 'Choose card to play! (' + cardsLeft + ' remaining)', {cancel: false});
      return hand[index];  
    }
  };
}
