var uuid = require('uuid');

module.exports = function(playerData) {
	var name = playerData.name;
  var team = playerData.team;
	var ship = playerData.currentShip;
  var id = uuid();
  
  function findInGameObjectInShip(id) {
    return null;
  }
  
  return {
    name: name,
    hand: [],
    team: team,
    ship: ship,
    maximumPlays: 3,
    id: id,
    setHand: function(newHand) { this.hand = newHand; },
    getInGameObject: function(id) {
      if (this.id == id) return this;
      else return findInGameObjectInShip(id);
    }
  };
};
