var fs = require('fs');

var Player = require('../player');
var Ship = require('../ship/ship');

function getProfileDirectory(username) {
	return './player-profiles/' + username;
}

function tryReadProfile(username) {
  try {   
    var path = getProfileDirectory(username) + '/' + username + '.player.json';
    var data = fs.readFileSync(path);
		return JSON.parse(data);
	}
	catch (error) {
    console.log(error);
    console.log('Couldn\'t load user profile for ' + username);
		return null;
	}
}

function tryReadShip(username, shipName) {
  try {
    var data = fs.readFileSync(getProfileDirectory(username) + '/' + shipName + '.ship.json');
		return JSON.parse(data);
	}
	catch (error) {
    console.log(error);
    console.log('Couldn\'t load player ship (' + username + '\'s ' + shipName + ')\n');
		return null;
	}
}

exports.loadPlayerByUserName = function(username) {
	if (getProfileDirectory(username)) {
		var profileData = tryReadProfile(username);
    profileData.currentShip = Ship(tryReadShip(username, profileData.currentShip));
		if (!profileData) return null;
    return Player(profileData);
	} else return null;
}
