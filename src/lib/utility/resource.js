var fs = require('fs');

module.exports = function() {
	var resources = [];
		
  function findResourceValue(name) {
		var targetString;
		resources.some(function(resource) {
		  if (resource[name]) {
				targetString = resource[name];
				return true;
			}
		});

		return targetString;
	}
		
	function processValue(value) {
		var regex = /%_(.+?)_%/;

		while (true) {
			var matched = value.match(regex);
			if (matched) {
				value = value.replace(matched[0], findResourceValue(matched[1], resources))
			} else break;
		}

		return value;
	}

	function processField(obj, field, resources) {
		var result = processValue(obj[field], resources);
		obj[field] = result;
	}

	function processArray(array) {
		for(var index = 0; index < array.length; index++) {
			array[index] = processValue(array[index], resources);	
		}
	}
		
	function formatObject(obj) {
		for(field in obj) {			
			if(typeof(obj[field]) == 'undefined') continue;
				
			var fieldType = typeof(obj[field]);
			if(fieldType == 'string') {
				processField(obj, field, resources);
			} else if (typeof(obj[field].length) != 'undefined') {
				processArray(obj[field], resources);
			} else if (fieldType == 'object') {
				formatObject(obj[field], resources);
			}
		}
		return obj;
	}

	return {
		loadResource: function(resourcePath) { resources.push(JSON.parse(fs.readFileSync(resourcePath, 'utf8'))); },
		processObject: formatObject,
		processString: processValue	
	};
}
