module.exports = function(rawEvents) {
  var events = rawEvents;

	function trigger(event, args) {
		var handlers = events[event] || [];
		handlers.forEach(function(handler) {
			handler(args);
		});
	}

	function on(event, handler) {
		var handlers = events[event];
		if (handlers) {
			handlers.push(handler);
		}
	}

  function detachEventHandler(event, handler) {
		var handlers = events[event];
		if (handlers) {
			var index = handlers.indexOf(event);
			if (index != -1) {
				handlers.splice(index, 1);
			}
		}	
	}
  
  return {
    on: on,
    trigger: trigger,
    detachEventHandler: detachEventHandler
  };
};