var Game = require('./lib/game');
var loader = require('./lib/utility/loader');
var Resource = require('./lib/utility/resource');
var CardLibrary = require('./lib/card/card-library');
var CardGenerator = require('./lib/card/card-generator');

(function launchGame() {
   var resource = Resource();
   
	 var cardLibrary = CardLibrary(['./cardpacks'], ['basic'], resource);
   var cardGenerator = CardGenerator(resource, cardLibrary);
   var game = Game(resource, cardGenerator);

   game.addPlayer(loader.loadPlayerByUserName('mokona'));
   game.addPlayer(loader.loadPlayerByUserName('qna'));
   game.start();
})();